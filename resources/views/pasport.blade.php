<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pasport</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Только CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">


    </head>
    <body>
        <style>
            .uper {
              margin-top: 40px;
            }
            .form-group {
                margin-bottom: 20px;
            }
        </style>

        <div class="container">
            <div class="card uper">
                <div class="card-header">
                    Данные технического паспорта
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form method="get" action="{{ route('pdf-create') }}">
                        <div class="form-group">
                            @csrf
                            <label for="name">Регистрационный номер</label>
                            <input type="text" class="form-control" name="reg_num"/>
                        </div>
                        <div class="form-group">
                            <label for="price">Марка, модель</label>
                            <input type="text" class="form-control" name="model"/>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="quantity">Категория (управления)</label>
                                    <input type="text" class="form-control" name="category_up"/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="quantity">Категория (ТС)</label>
                                    <input type="text" class="form-control" name="category_tc"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="quantity">Дата выдачи свидетельства</label>
                            <input type="text" class="form-control" name="date_get"/>
                        </div>

                        <div class="form-group">
                            <label for="quantity">VIN / кузов / шасси</label>
                            <input type="text" class="form-control" name="vin"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Год выпуска ТС</label>
                            <input type="text" class="form-control" name="date_tc"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Обьем двиготеля, см.куб.</label>
                            <input type="text" class="form-control" name="engine_size"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Цвет</label>
                            <input type="text" class="form-control" name="color"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Масса без нагрузки, кг</label>
                            <input type="text" class="form-control" name="massa_min"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Разрешонная максимальная масса, кг</label>
                            <input type="text" class="form-control" name="massa_max"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Владелец</label>
                            <input type="text" class="form-control" name="owner"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Владелец транскрипция</label>
                            <input type="text" class="form-control" name="owner_tr"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Место жительства (область)</label>
                            <input type="text" class="form-control" name="oblast"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Транкрипция места жительства (область)</label>
                            <input type="text" class="form-control" name="oblast_tr"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Особые метки</label>
                            <input type="text" class="form-control" name="tags"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Отметка о снятии с учета</label>
                            <input type="text" class="form-control" name="date_delete_tc"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Серия и регистрационный номер свидетельства</label>
                            <input type="text" class="form-control" name="series"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Номер на обратной стороне</label>
                            <input type="text" class="form-control" name="series_obr"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Скачать в pdf</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
