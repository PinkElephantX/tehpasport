<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pasport</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Только CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">


    </head>
    <body>
        <style>
            .uper {
              margin-top: 40px;
            }
            .form-group {
                margin-bottom: 20px;
            }
        </style>

        <div class="container">
            <div class="card uper  w-50 mx-auto">
                <div class="card-header">
                    Войти в сервис
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{ route('auth') }}"  method="post" accept-charset="utf-8">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="name">Логин</label>
                            <input type="text" class="form-control" name="login" required/>
                        </div>
                        <div class="form-group">
                            <label for="price">Пароль</label>
                            <input type="password" class="form-control" name="password" required/>
                        </div>
                        <button type="submit" class="btn btn-primary">Войти</button>
                        <div class="d-flex float-end label mt-2" style="color:silver;">- Для воствновления кода обратитесь к администратору -</div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
