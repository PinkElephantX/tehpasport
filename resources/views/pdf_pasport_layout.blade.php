<!doctype html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Техпаспорт</title>
</head>










{{-- $date_delete_tc- --}}

<body>
<div class="case" style="width: 794px;">
    <span class="eng r1 p1">{{ $reg_num }}</span>
    <span class="eng r1 p1x">{{ $reg_num }}</span>
    <span class="eng r11 p1">{{ $reg_num }}</span>
    <span class="eng r11 p1x">{{ $reg_num }}</span>

    <span class="eng r1 p2">{{ $model }}</span>
    <span class="eng r1 p2x">{{ $model }}</span>
    <span class="eng r11 p2">{{ $model }}</span>
    <span class="eng r11 p2x">{{ $model }}</span>

    <span class="eng r1 p3">{{ $category_up }}<span class="eng unset">/</span>{{ $category_tc }}</span>
    <span class="eng r1 p3x">{{ $category_up }}<span class="eng unset">/</span>{{ $category_tc }}</span>
    <span class="eng r11 p3">{{ $category_up }}<span class="eng unset">/</span>{{ $category_tc }}</span>
    <span class="eng r11 p3x">{{ $category_up }}<span class="eng unset">/</span>{{ $category_tc }}</span>

    <span class="eng r2 p4">{{ $date_get }}</span>
    <span class="eng r2 p4x">{{ $date_get }}</span>
    <span class="eng r22 p4">{{ $date_get }}</span>
    <span class="eng r22 p4x">{{ $date_get }}</span>

    <span class="eng r2 p5">{{ $vin }}</span>
    <span class="eng r2 p5x">{{ $vin }}</span>
    <span class="eng r22 p5">{{ $vin }}</span>
    <span class="eng r22 p5x">{{ $vin }}</span>

    <span class="eng r2 p6">{{ $date_tc }}</span>
    <span class="eng r2 p6x">{{ $date_tc }}</span>
    <span class="eng r22 p6">{{ $date_tc }}</span>
    <span class="eng r22 p6x">{{ $date_tc }}</span>

    <span class="eng r2 p7">{{ $engine_size }}</span>
    <span class="eng r2 p7x">{{ $engine_size }}</span>
    <span class="eng r22 p7">{{ $engine_size }}</span>
    <span class="eng r22 p7x">{{ $engine_size }}</span>

    <span class="eng r3 p8">{{ $color }}<span class="eng unset">/</span></span>

    <span class="eng r3 p9">{{ $massa_min }}</span>
    <span class="eng r3 p9x">{{ $massa_min }}</span>
    <span class="eng r33 p9">{{ $massa_min }}</span>
    <span class="eng r33 p9x">{{ $massa_min }}</span>

    <span class="eng r3 p10">{{ $massa_max }}</span>
    <span class="eng r3 p10x">{{ $massa_max }}</span>
    <span class="eng r33 p10">{{ $massa_max }}</span>
    <span class="eng r33 p10x">{{ $massa_max }}</span>

    <span class="rus r4 p11">{{ $owner }} <span class="eng unset">/</span></span>
    <span class="rus r4 p11x">{{ $owner }} <span class="eng unset">/</span></span>
    <span class="rus r44 p11">{{ $owner }} <span class="eng unset">/</span></span>
    <span class="rus r44 p11x">{{ $owner }} <span class="eng unset">/</span></span>

    <span class="eng r5 p11">{{ $owner_eng }}</span>
    {{-- <span class="eng r5 p11x">{{ $owner_eng }}</span> --}}
    {{-- <span class="eng r55 p11">{{ $owner_eng }}</span> --}}
    <span class="eng r55 p11x">{{ $owner_eng }}</span>

    <span class="r6 p12">
        <span class="rus unset">{{ $oblast }}</span>
        <span class="eng unset">/ {{ $oblast_eng }}</span>
    </span>

    <span class="eng r7 p13">{{ $tags }}</span>

    <span class="eng r8 p14">{{ $series }}</span>
    <span class="eng r8 p14x">{{ $series }}</span>
    <span class="eng r88 p14">{{ $series }}</span>
    <span class="eng r88 p14x">{{ $series }}</span>

    <span class="rus r9 p15">{{ $series_obr }}</span>
    <img
        src="{{asset('/images/tex-pasport.jpg')}}"
        style="
            width: 794px;
            position: absolute;
            top: 0;
            left: 0;">
</div>


<style>

    .eng {
        position: fixed;
        z-index: 999;
        font-family: tahoma;
        font-size: 21px;
        letter-spacing: -0.5px;
        text-transform: uppercase;
    }
    .rus{
        position: fixed;
        z-index: 999;
        font-size: 21px;
        font-family: Arial Narrow;
        letter-spacing: 0.7px;
        text-transform: uppercase;
    }
    .unset {
        position: unset;
    }

    .r1 {padding-top: 152px;}
    .r11 {padding-top: 152.5px;}
    .p1 {padding-left: 86px;}
    .p1x {padding-left: 86.5px;}
    .p2 {padding-left: 236px;}
    .p2x {padding-left: 236.5px;}
    .p3 {padding-left: 606px;}
    .p3x {padding-left: 606.5px;}

    .r2 {padding-top: 191px;}
    .r22 {padding-top: 191.5px;}
    .p4 {padding-left: 86px;}
    .p4x {padding-left: 86.5px;}
    .p5 {padding-left: 236px;}
    .p5x {padding-left: 236.5px;}
    .p6 {padding-left: 606px;}
    .p6x {padding-left: 606.5px;}
    .p7 {padding-left: 679px;}
    .p7x {padding-left: 679.5px;}

    .r3 {padding-top: 231px;}
    .r33 {padding-top: 231.5px;}
    .p8 {padding-left: 86px;}
    .p9 {padding-left: 606px;}
    .p9x {padding-left: 606.5px;}
    .p10 {padding-left: 690px;}
    .p10x {padding-left: 690.5px;}

    .r4 {padding-top: 270px;}
    .r44 {padding-top: 270.5px;}

    .r5 {padding-top: 298px;}
    .r55 {padding-top: 298.5px;}
    .p11 {padding-left: 98px;}
    .p11x {padding-left: 98.5px;}

    .r6 {
        padding-top: 338px;
        position: fixed;
        z-index: 999;
    }
    .p12 {padding-left: 98px;}

    .r7 {padding-top: 377px;}
    .p13 {padding-left: 98px;}

    .r8 {padding-top: 460px;}
    .r88 {padding-top: 460.5px;}
    .p14 {padding-left: 377px;}
    .p14x {padding-left: 377.5px;}

    .r9 {
        padding-top: 901px;
        letter-spacing: 1.8px;
    }
    .p15 {padding-left: 349px;}


    .pricerows > div:nth-child(2n) {
        background-color: #f0f0f0;
    }

    @font-face {
    font-family: 'Arial Narrow';
    src: url("{{ public_path() . '/fonts/ArialNarrow.eot' }}");
    src: url("{{ public_path() . '/fonts/ArialNarrow.eot?#iefix' }}") format('embedded-opentype'),
        url("{{ public_path() . '/fonts/ArialNarrow.woff' }}") format('woff'),
        url("{{ public_path() . '/fonts/ArialNarrow.ttf' }}") format('truetype');
    font-weight: normal;
    font-style: normal;
    }

    @font-face {
        font-family: "tahoma";
        src: url("{{ public_path() . '/fonts/Tahoma.eot' }}");
        src: url("{{ public_path() . '/fonts/Tahoma.eot?#iefix' }}") format("embedded-opentype"),
        url("{{ public_path() . '/fonts/Tahoma.woff2' }}") format("woff2"),
        url("{{ public_path() . '/fonts/Tahoma.woff' }}") format("woff"),
        url("{{ public_path() . '/fonts/Tahoma.ttf' }}") format("truetype"),
        url("{{ public_path() . '/fonts/Tahoma.svg#tahoma' }}") format("svg");
    }

    * {
        margin: 0;
        padding: 0;
    }

    .header {
        color: #cc0000;
        font-weight: bold;
    }

</style>

</body>

</html>
