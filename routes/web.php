<?php

use Illuminate\Support\Facades\Route;

use App\User;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/pasport/download',"Controller@pdf")->name("pdf-create")->middleware('auth');
Route::get('/pasport',"Controller@pasport")->name('pasport')->middleware('auth');
Route::post('/checkuser',"Controller@checkuser")->name('auth');
Route::post('/saveuser',"Controller@saveuser")->name('save');
Route::get('/adduser',"Controller@adduser")->name('adduser');

Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::get('/test', function () {
    User::create([
        'login' => 'admin',
        'password' => Hash::make('ByBrJv390&&&'),
        'role' => 'admin'
    ]);
});


