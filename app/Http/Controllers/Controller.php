<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf as PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function pdf(Request $request){
        if(!Auth::check()){
            return redirect('/');
        }
        // $Data = $request->validate([
        //     'reg_num' => 'required|alpha_num|max:80',
        //     'model' => 'required|alpha_num|max:60',
        //     'category_tc' => 'required|alpha_num|max:10',
        //     'date_get' => 'required|date|date_format:d.m.Y',
        //     'vin' => 'required|alpha_num|max:255',
        //     'date_tc' => 'required|date|date_format:Y',
        //     'engine_size' => 'required|integer|min:0',
        //     'color' => 'required|max:255',
        //     'massa_min' => 'required|integer|min:0',
        //     'massa_max' => 'required|integer|min:0',
        //     'owner' => 'required|max:255|alpha_dash',
        //     'tags' => 'max:100',
        //     'date_delete_tc' => 'date|date_format:d.m.Y',
        //     'series' => 'required|alpha_num|max:255',
        // ])->validate();
        $Data = $request->validate([
            'reg_num' => 'required',
            'model' => 'required',
            'category_tc' => 'required',
            'category_up' => 'required',
            'date_get' => 'required',
            'vin' => 'required',
            'date_tc' => 'required',
            'engine_size' => 'required',
            'color' => 'required',
            'massa_min' => 'required',
            'massa_max' => 'required',
            'owner' => 'required',
            'owner_tr' => 'required',
            'oblast' => 'required',
            'oblast_tr' => 'required',
            'tags' => 'max:100',
            'date_delete_tc' => 'max:100',
            'series' => 'required',
            'series_obr' => 'required',
        ]);

        $reg_num = $Data['reg_num'];
        $model = $Data['model'];
        $category_tc = $Data['category_tc'];
        $category_up = $Data['category_up'];
        $date_get = $Data['date_get'];
        $vin = $Data['vin'];
        $date_tc = $Data['date_tc'];
        $engine_size = $Data['engine_size'];
        $color = $Data['color'];
        $massa_min = $Data['massa_min'];
        $massa_max = $Data['massa_max'];
        $owner = $Data['owner'];
        $oblast = $Data['oblast'];
        $tags = $Data['tags'];
        $date_delete_tc = $Data['date_delete_tc'];
        $series = $Data['series'];
        $owner_eng = $Data['owner_tr'];
        $oblast_eng = $Data['oblast_tr'];
        $series_obr = $Data['series_obr'];

        $pdf = PDF::loadView('pdf_pasport_layout',  compact('reg_num', 'oblast', 'oblast_eng', 'owner_eng', 'model', 'category_up', 'category_tc', 'date_get', 'vin', 'date_tc', 'engine_size', 'color', 'massa_min', 'massa_max', 'owner', 'tags', 'date_delete_tc', 'series', 'series_obr'));

        return $pdf->download("56262347247247247.pdf");
    }

    function pasport(Request $request){
        return view("pasport");
    }

    function checkuser(Request $request){
        $userdata = $request->validate([
            'login' => 'required',
            'password' => 'required',
        ]);

        $login = $userdata['login'];
        $password = $userdata['password'];


        $authCheck = Auth::attempt([
            'login' => $login ,
            'password' => $password
        ], true);

        if($authCheck){
            $user = User::where('login', $login)->first();
            if($user->role == 'admin'){
                return view("register");
            }else{
                return redirect('/pasport');
            }

        }
        else {
            return redirect('/');
        }
    }
    function adduser(){
        return view("register");
    }

    function saveuser(Request $request){
        $userdata = $request->validate([
            'login' => 'required',
            'password' => 'required',
            'role' => 'required',
        ]);

        $user = User::where('login', $userdata['login'])->first();
        if(!$user){
            User::create([
                'login' => $userdata['login'],
                'password' => Hash::make($userdata['password']),
                'role' => $userdata['role'],
            ]);
            return view("register");
        }
        return "<h3>Данный польователь уже существует</h3>";
    }
}
